﻿using Dapper;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BlazorApp.Entities
{
    public interface IVentilatoreSQL
    {
        public int Id { get; set; }
        public string Serie { get; set; }
        public string Modello { get; set; }
        public bool Trasmissione { get; set; }
        public string Esecuzione { get; set; }
        public string Rotazione { get; set; }

        public void init() { }
        public void caricaDaDB() { }
    }

    public class VentilatoreSQL : IVentilatoreSQL
    {
        [Key]
        public int Id { get; set; }
        public string Serie { get; set; }
        public string Modello { get; set; }
        public bool Trasmissione { get; set; } = false;
        public string Esecuzione { get; set; }
        public string Rotazione { get; set; }

        public List<Accessorio> AccessoriAspirazione = new List<Accessorio>();
        public List<Accessorio> AccessoriAspirazioneOrdinati = new List<Accessorio>();
        public List<Accessorio> AccessoriMandata = new List<Accessorio>();
        public List<Accessorio> AccessoriMandataOrdinati = new List<Accessorio>();
        public List<string> EsecuzioniDirette = new List<string>();
        public List<string> EsecuzioniTrasmissione = new List<string>();

        public void init()
        {
            caricaDaDB();
        }

        public void caricaDaDB()
        {
            using (System.Data.IDbConnection db = new SqlConnection("Data Source=ERALDO-CIT\\SQLEXPRESS;Initial Catalog=TestMZ;Integrated Security=True"))
            {
                EsecuzioniDirette = db.Query<string>("Select Esecuzione From Esecuzioni where Tipo = 'Diretto'").ToList();
                EsecuzioniTrasmissione = db.Query<string>("Select Esecuzione From Esecuzioni where Tipo = 'Trasmissione'").ToList();
                AccessoriAspirazione = db.Query<Accessorio>("Select Codice From AccessoriAspirazione").ToList();
                AccessoriMandata = db.Query<Accessorio>("Select Codice From AccessoriMandata").ToList();
            }
            foreach (Accessorio acc in AccessoriAspirazione)
            {
                acc.Direzione = "Aspirazione";
            }
            foreach (Accessorio acc in AccessoriMandata)
            {
                acc.Direzione = "Mandata";
            }
        }


    }
}
