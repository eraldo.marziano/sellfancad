﻿using System.Collections.Generic;

namespace BlazorApp.Entities
{
    public class OperationParameter
    {
        public OperationParameter()
        {

        }
        public OperationParameter(string _Name, string _Value)
        {
            Name = _Name;
            Value = _Value;
        }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Operation
    {
        public Operation()
        {
            Parameters = new List<OperationParameter>();
        }
        public Operation(string _Name)
        {
            Name = _Name;
            Parameters = new List<OperationParameter>();
        }
        public string Name { get; set; }
        public List<OperationParameter> Parameters { get; set; }
    }

    public class ProcessingSettings
    {
        public ProcessingSettings()
        {

        }
        public string PlatformName { get; set; } = "INVENTOR";
        public string LengthUnits { get; set; } = "milimeters";
        public string AngleUnits { get; set; } = "degrees";
        public bool IsSessionVisible { get; set; } = false;
        public bool UpdateAfterEachOperation { get; set; } = false;
    }
        public class AdditionalInfo
    {
        public AdditionalInfo()
        {

        }
        public string NotificationEmail { get; set; } = "eraldo.marziano@configuratori.com";
        public string UserID { get; set; } = "z0037p6s";
        public string ClientRequestID { get; set; } = "GRO000070_0003 | MCZ23_1";
        public bool WriteItemIDsInDatabase { get; set; } = true;
    }

    public class IntegrationInput
    {
        public IntegrationInput()
        {
            Operations = new List<Operation>();
            ProcessingSettings = new ProcessingSettings();
            AdditionalInfo = new AdditionalInfo();
        }
        public List<Operation> Operations { get; set; }
        public ProcessingSettings ProcessingSettings { get; set; }
        public AdditionalInfo AdditionalInfo { get; set; }
    }
}
