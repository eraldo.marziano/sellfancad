﻿using System;

namespace BlazorApp.Entities
{
    public class Accessorio
    {
        public string Codice { get; set; }
        public Boolean Selezionato { get; set; } = false;
        public Accessorio Clone()
        {
            return (Accessorio)this.MemberwiseClone();
        }
        public int Quantita { get; set; } = 0;
        public int Id { get; set; }

        public string Direzione { get; set; }
        public override string ToString()
        {
            return Codice + " - " + Id;
        }
    }
}
