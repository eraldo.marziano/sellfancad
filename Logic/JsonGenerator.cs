﻿using BlazorApp.Entities;
using log4net;
using log4net.Config;
using Microsoft.Data.SqlClient;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Data;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using System.Collections.Generic;

namespace BlazorApp.Logic
{
    public class JsonGenerator
    {

        private ILog _log;
        private VentilatoreSQL ventilatore;
        private DataTable infoVentilatore;
        private DataRow datiEsecuzione;
        private string percorsoCompleto = "C:\\_DEVELOPMENT\\CAD_SERVER\\CAD_TEMPLATES\\";

        public JsonGenerator(VentilatoreSQL v, ILog log)
        {
            ventilatore = v;
            _log = log;
        }
        public void generaJSon()
        {
            XmlConfigurator.Configure();
            ReadCsv();
            BuildJson();
        }
        void ReadCsv()
        {
            string fileName = Properties.Resources.Data + ventilatore.Modello.Replace(" ", "_");
            using (FileStream fs = new FileStream(fileName + ".csv", FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    using var csvReader = new CsvReader(sr, true, ';');
                    infoVentilatore = new DataTable();
                    infoVentilatore.Load(csvReader);
                }
                string expression;
                expression = "Esecuzione = '" + ventilatore.Esecuzione + "'";
                datiEsecuzione = infoVentilatore.Select(expression)[0];
            }
        }
        private void BuildJson()
        {
            IntegrationInput json = new IntegrationInput();
            ScriviIntro(json);
            ScriviParametriPrincipali(json);
            ScriviParametriBoccaAspirazione(json);
            ScriviParametriBoccaMandata(json);
            ScriviAccessoriAspirazione(json);
            ScriviAccessoriMandata(json);
            Serializza(json);
            SendRequest(json);

        }
        private void ScriviIntro(IntegrationInput json)
        {
            Operation open = new Operation("OpenDocument");
            open.Parameters.Add(new OperationParameter("fileFullName", percorsoCompleto + "E04.iam"));
            json.Operations.Add(open);
        }
        private void ScriviParametriPrincipali(IntegrationInput json)
        {
            for (int i = 1; i < datiEsecuzione.Table.Columns.Count; i++)
            {
                if (!datiEsecuzione[i].Equals("-") && !datiEsecuzione.Table.Columns[i].ColumnName.StartsWith("Bocca"))
                {
                    Operation modifyPar = new Operation("ModifyParameter");
                    modifyPar.Parameters.Add(new OperationParameter("componentName", "chiocciola:1"));
                    modifyPar.Parameters.Add(new OperationParameter("parameterName", "dim_" + datiEsecuzione.Table.Columns[i].ColumnName));
                    modifyPar.Parameters.Add(new OperationParameter("parameterValue", datiEsecuzione[i].ToString()));
                    json.Operations.Add(modifyPar);
                }
            }
        }
        private void ScriviParametriBoccaAspirazione(IntegrationInput json)
        {
            try
            {
                Operation setNumeroSpine = new Operation("ModifyParameter");
                setNumeroSpine.Parameters.Add(new OperationParameter("componentName", "Bocca:1"));
                setNumeroSpine.Parameters.Add(new OperationParameter("parameterName", "dim_NumeroSpine"));
                setNumeroSpine.Parameters.Add(new OperationParameter("parameterValue", datiEsecuzione["BoccaForiAspirazione"].ToString()));
                json.Operations.Add(setNumeroSpine);

                using (SqlConnection connection = new SqlConnection(Properties.Resources.Connection))
                {
                    string query = string.Format("Select * From Bocca where d1 ='{0}'", datiEsecuzione["BoccaAspirazione"].ToString());
                    SqlCommand cm = new SqlCommand(query, connection);
                    connection.Open();
                    SqlDataReader sdr = cm.ExecuteReader();
                    sdr.Read();
                    for (int j = 1; j < sdr.FieldCount; j++)
                    {
                        Operation modifyAccessorio = new Operation("ModifyParameter");
                        modifyAccessorio.Parameters.Add(new OperationParameter("componentName", "Bocca:1"));
                        modifyAccessorio.Parameters.Add(new OperationParameter("parameterName", "dim_" + sdr.GetName(j)));
                        modifyAccessorio.Parameters.Add(new OperationParameter("parameterValue", sdr.GetString(j)));
                        json.Operations.Add(modifyAccessorio);
                    }
                }
            } catch (Exception e)
            {
                _log.Error(e.Message);
            }
        }
        private void ScriviParametriBoccaMandata(IntegrationInput json)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Properties.Resources.Connection))
                {
                    string query = string.Format("Select * From BoccaMandata where axb ='{0}'", datiEsecuzione["BoccaMandata"].ToString());
                    SqlCommand cm = new SqlCommand(query, connection);
                    connection.Open();
                    SqlDataReader sdr = cm.ExecuteReader();
                    sdr.Read();
                    for (int j = 2; j < sdr.FieldCount; j++)
                    {
                        if (!sdr.GetName(j).Equals("phi"))
                        {
                            Operation modifyAccessorio = new Operation("ModifyParameter");
                            modifyAccessorio.Parameters.Add(new OperationParameter("componentName", "chiocciola:1"));
                            modifyAccessorio.Parameters.Add(new OperationParameter("parameterName", "bocca_" + sdr.GetName(j)));
                            modifyAccessorio.Parameters.Add(new OperationParameter("parameterValue", sdr.GetString(j)));
                            json.Operations.Add(modifyAccessorio);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message);
            }
        }
        private void ScriviAccessoriAspirazione(IntegrationInput json)
        {
            string componente1 = "bocca:1";
            string dimensioneFlangia = datiEsecuzione["BoccaAspirazione"].ToString();
            string foriAspirazione = datiEsecuzione["BoccaForiAspirazione"].ToString();
            var listaAccessori = ventilatore.AccessoriAspirazioneOrdinati;
            Dictionary<string, int> contatoreAccessori = new Dictionary<string, int>();
            foreach (Accessorio accessorio in listaAccessori) 
            {
                if (contatoreAccessori.ContainsKey(accessorio.Codice))
                    contatoreAccessori[accessorio.Codice] += 1;
                else
                    contatoreAccessori.Add(accessorio.Codice, 1);
                string idAccessorio = accessorio.Codice + ":" + contatoreAccessori[accessorio.Codice];
                Operation addAccessorio = new Operation("AddComponent");
                addAccessorio.Parameters.Add(new OperationParameter("componentFullName", percorsoCompleto + accessorio.Codice + ".ipt"));
                Operation matchAccessorio = new Operation("MateCoordinateSystems");
                matchAccessorio.Parameters.Add(new OperationParameter("component1Name", componente1));
                matchAccessorio.Parameters.Add(new OperationParameter("component2Name", idAccessorio));
                matchAccessorio.Parameters.Add(new OperationParameter("coordinateSystem1Name", "UCSOUT"));
                matchAccessorio.Parameters.Add(new OperationParameter("coordinateSystem2Name", "UCSIN"));
                json.Operations.Add(addAccessorio);
                json.Operations.Add(matchAccessorio);
                try
                {
                    using (SqlConnection connection = new SqlConnection(Properties.Resources.Connection))
                    {
                        // Creating SqlCommand objcet   
                        string query = string.Format("Select * From {0} where d1 ='{1}'", accessorio.Codice, dimensioneFlangia);
                        SqlCommand cm = new SqlCommand(query, connection);
                        connection.Open();
                        SqlDataReader sdr = cm.ExecuteReader();
                        sdr.Read();
                        for (int j = 1; j < sdr.FieldCount; j++)
                        {
                            Operation modifyAccessorio = new Operation("ModifyParameter");
                            modifyAccessorio.Parameters.Add(new OperationParameter("componentName", idAccessorio));
                            modifyAccessorio.Parameters.Add(new OperationParameter("parameterName", "dim_" + sdr.GetName(j)));
                            modifyAccessorio.Parameters.Add(new OperationParameter("parameterValue", sdr.GetString(j)));
                            json.Operations.Add(modifyAccessorio);
                        }
                        Operation modifyAccessorioFori = new Operation("ModifyParameter");
                        modifyAccessorioFori.Parameters.Add(new OperationParameter("componentName", idAccessorio));
                        modifyAccessorioFori.Parameters.Add(new OperationParameter("parameterName", "dim_NumeroFori"));
                        modifyAccessorioFori.Parameters.Add(new OperationParameter("parameterValue", foriAspirazione));
                        json.Operations.Add(modifyAccessorioFori);
                        dimensioneFlangia = sdr["d1"].ToString();
                        componente1 = idAccessorio;
                    }
                } catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }

        private void ScriviAccessoriMandata(IntegrationInput json)
        {
            string componente1 = "chiocciola:1";
            string dimensioneFlangia = datiEsecuzione["BoccaMandata"].ToString();
            string nomeUCS = "UCSMandata";
            var listaAccessori = ventilatore.AccessoriMandataOrdinati;
            Dictionary<string, int> contatoreAccessori = new Dictionary<string, int>();
            foreach (Accessorio accessorio in listaAccessori)
            {
                if (contatoreAccessori.ContainsKey(accessorio.Codice))
                    contatoreAccessori[accessorio.Codice] += 1;
                else
                    contatoreAccessori.Add(accessorio.Codice, 1);
                string idAccessorio = accessorio.Codice + ":" + contatoreAccessori[accessorio.Codice];
                Operation addAccessorio = new Operation("AddComponent");
                addAccessorio.Parameters.Add(new OperationParameter("componentFullName", percorsoCompleto + accessorio.Codice + ".ipt"));
                Operation matchAccessorio = new Operation("MateCoordinateSystems");
                matchAccessorio.Parameters.Add(new OperationParameter("component1Name", componente1));
                matchAccessorio.Parameters.Add(new OperationParameter("component2Name", idAccessorio));
                matchAccessorio.Parameters.Add(new OperationParameter("coordinateSystem1Name", nomeUCS));
                matchAccessorio.Parameters.Add(new OperationParameter("coordinateSystem2Name", "UCSIN"));
                json.Operations.Add(addAccessorio);
                json.Operations.Add(matchAccessorio);
                nomeUCS = "UCSOUT";

                try
                {
                    using (SqlConnection connection = new SqlConnection(Properties.Resources.Connection))
                    {
                        // Creating SqlCommand objcet   
                        string query = string.Format("Select * From {0} where axb ='{1}'", accessorio.Codice, dimensioneFlangia);
                        SqlCommand cm = new SqlCommand(query, connection);
                        connection.Open();
                        SqlDataReader sdr = cm.ExecuteReader();
                        sdr.Read();
                        for (int j = 2; j < sdr.FieldCount; j++)
                        {
                            Operation modifyAccessorio = new Operation("ModifyParameter");
                            modifyAccessorio.Parameters.Add(new OperationParameter("componentName", idAccessorio));
                            modifyAccessorio.Parameters.Add(new OperationParameter("parameterName", "dim_" + sdr.GetName(j)));
                            modifyAccessorio.Parameters.Add(new OperationParameter("parameterValue", sdr.GetString(j)));
                            json.Operations.Add(modifyAccessorio);
                        }
                        dimensioneFlangia = sdr["axb"].ToString();
                        componente1 = idAccessorio;
                    }
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                }
            }
        }
        private void Serializza(IntegrationInput json)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(json.GetType());
            x.Serialize(System.Console.Out, json);
            using (StreamWriter writer = new StreamWriter("C:\\Users\\Eraldo Marziano\\Desktop\\test\\file.xml"))
            {
                x.Serialize(writer, json);
            }
        }
        private void SendRequest(IntegrationInput json)
        {
            var client = new RestClient("http://95.217.32.147/api/processing");
            client.Authenticator = new HttpBasicAuthenticator("demouser", "Configuratori.it");
            var request = new RestRequest("")
            .AddXmlBody(json);
            var response = client.Post(request);
            _log.Info(string.Format("Request sent - Response status: {0} - Response content: {1}", response.StatusCode, response.Content));

        }
    }
}
